def calculate_square_random_number(random_number):
    return random_number ** 2


def convert_temperature_from_Celsius_to_fahrenheit(celsius_temp):
    return celsius_temp * 1.8 + 32


def calculate_volume_of_a_cuboid(a, b, c):
    return a * b * c


if __name__ == '__main__':
    # Zadanie 1
    print(calculate_square_random_number(0))
    print(calculate_square_random_number(16))
    print(calculate_square_random_number(25))

    # Zadanie 2
    print(convert_temperature_from_Celsius_to_fahrenheit(20))

    # Zadanie 3
    print("Volume of a cuboid is:", calculate_volume_of_a_cuboid(3, 5, 7))
