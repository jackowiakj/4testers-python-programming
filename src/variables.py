first_name = "Justyna"
last_name = "Jackowiak"
age = 32

print(first_name)
print(last_name)
print(age)
# #Below I describe my friendship details as a set of variables
name = "Radek"
age_in_years = 33
amount_of_animals = 3
has_driving_license = True
friendship_duration_in_years = 10.5
print(name, age_in_years, amount_of_animals, has_driving_license, friendship_duration_in_years, sep = ';')
