def print_greetings_for_a_person_in_the_city(name, city):
    print(f"Witaj {name}! Miło Cię widzieć w naszym mieście: {city}!")


def generate_email_for_4testers(first_name, last_name):
    print(f"{first_name.lower()}.{last_name.lower()}@4testers.pl")


if __name__ == '__main__':
    print_greetings_for_a_person_in_the_city("Justyna", "Poznań")
    generate_email_for_4testers("Janusz", "Nowak")
    generate_email_for_4testers("Barbara", "Kowalska")
