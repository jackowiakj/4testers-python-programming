animals = [
    {'name': 'Burek', 'kind': 'dog', 'age': 7},
    {'name': 'Bonifacy', 'kind': 'cat', 'age': None},
    {'name': 'Misio', 'kind': 'hamster', 'age': 1},
]
print(animals[-1], ['name'])
animals[1]['age'] = 2
print(animals[1])
animals.insert(0, {'name': 'Reksio', 'kind': 'dog', 'age': 9})
print(animals)

adresses = [
    {'city': 'Poznań', 'street': 'Polska', 'house_number': 11, 'post_code': '60-161'},
    {'city': 'Warszawa', 'street': 'Długa', 'house_number': 4, 'post_code': '64-234'},
    {'city': 'Wrocław', 'street': 'Polna', 'house_number': 9, 'post_code': '82-130'},
]

if __name__ == '__main__':

print(adresses[-1]['post_code'])
print(adresses[1]['city'])
adresses[0]['street'] = 'Wodna'
print(adresses)
