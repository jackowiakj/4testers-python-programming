def display_speed_information(speed):
    #  if speed <= 50:
    #     print('Thank you, tour speed is below limit! :)')
    #  else:
    #     print("Your speed is to high. Slow down!")
    if speed > 50:
        print("Your speed is to high. Slow down!")
    else:
        print('Thank you, tour speed is below limit! :)')


def normal_conditions_of_atmosphere(temperature_in_celsius_degree, air_pressure_in_hectopascals):
    if temperature_in_celsius_degree == 0 and air_pressure_in_hectopascals == 1013:
        return True
    else:
        return False


def calculate_fine_amount(speed):
    return 500 + (speed - 50) * 10


def print_the_value_of_speeding_fine_in_built_up_area(speed):
    print('Your speed was:', speed)

    if speed > 100:
        print('You just lost your driving license')
    elif speed > 50:
        print(f'You just got a fine! Fine amount {calculate_fine_amount(speed)}')
    else:
        print('Thank you, tour speed is fine')


def return_school_degrees(evaluation):
    if not (isinstance(evaluation, float) or isinstance(evaluation, int)):
        return 'N/A'
    elif evaluation < 2 or evaluation > 5:
        return 'N/A'
    elif evaluation >= 4.5:
        return 'bardzo dobry'
    elif evaluation >= 4.0:
        return 'dobry'
    elif evaluation >= 3:
        return 'dostateczny'
    else:
        return 'niedostateczny'


if __name__ == '__main__':
    display_speed_information(50)
    display_speed_information(49)
    display_speed_information(51)

    print(normal_conditions_of_atmosphere(0, 1013))
    print(normal_conditions_of_atmosphere(0, 1014))
    print(normal_conditions_of_atmosphere(1, 1013))
    print(normal_conditions_of_atmosphere(1, 1014))

    print_the_value_of_speeding_fine_in_built_up_area(101)
    print_the_value_of_speeding_fine_in_built_up_area(100)
    print_the_value_of_speeding_fine_in_built_up_area(51)
    print_the_value_of_speeding_fine_in_built_up_area(50)
    print_the_value_of_speeding_fine_in_built_up_area(60)

    print(return_school_degrees(4.6))
    print(return_school_degrees(4.5))
    print(return_school_degrees(4))
    print(return_school_degrees(3.9))
    print(return_school_degrees(6))
    print(return_school_degrees(2))
    print(return_school_degrees(1))
