from src.oop.household_items import Cup


def test_fill_cup_over_the_maximum_volume():
    test_cup = Cup(500)
    test_cup.fill_the_cup(550)
    assert test_cup.current_liquid_volume == 500


def test_fill_cup_under_the_aximum_volume():
    test_cup = Cup(500)
    test_cup.fill_the_cup(450)
    assert test_cup.current_liquid_volume == 450

def test_fill_cup_twice():
    test_cup = Cup(500)
    test_cup.fill_the_cup(450)
    test_cup.fill_the_cup(100)
    assert test_cup.current_liquid_volume == 500
