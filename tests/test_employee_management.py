from src.employee_management import get_dictionary_with_random_personal_data
from src.employee_management import generate_seniority_years


def test_single_employee_generated_has_proper_keys():
    test_employee = get_dictionary_with_random_personal_data()
    assert 'email' in test_employee.keys()
    assert 'seniority_years' in test_employee.keys()
    assert 'female' in test_employee.keys()


def test_generated_seniority_years_are_in_proper_range():
    test_seniority_years = generate_seniority_years()
    assert test_seniority_years >= 0
    assert test_seniority_years <= 40
    assert isinstance(test_seniority_years, int)
